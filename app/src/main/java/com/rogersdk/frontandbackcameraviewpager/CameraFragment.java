package com.rogersdk.frontandbackcameraviewpager;

import android.app.Activity;
import android.hardware.Camera;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

/**
 * Created by rogerio on 16/12/15.
 */
public class CameraFragment extends Fragment {

    public static final String IS_CAMERA_FRONTAL = "isFrontal";
    private FrameLayout mCameraFragmentView;
    private CameraPreview mPreview;
    private Camera mCamera;
    private CameraFragmentCallback mCallback;
    private boolean isFrontal;

    public CameraFragment() {
    }

    public static CameraFragment newInstance(boolean isFrontal) {
        CameraFragment fragment = new CameraFragment();
        Bundle args = new Bundle();
        args.putBoolean(IS_CAMERA_FRONTAL, isFrontal);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_camera, container, false);

        TextView title = (TextView) view.findViewById(R.id.title);

        Bundle args = getArguments();
        isFrontal = false;

        if(!args.isEmpty()) {
            //TODO implementar o fragment da camera
            isFrontal = args.getBoolean(IS_CAMERA_FRONTAL, false);
        }

        title.setText(String.format("Camera is %s", isFrontal ? "Frontal" : "Traseira"));

        mCameraFragmentView = (FrameLayout) view.findViewById(R.id.camera_fragment_view);

        /*if(!mCallback.cameraIsLocked()) {
            Log.d("visible", "passou no unlock " + this);
            mCallback.lockCamera();
            mCamera = findCamera(isFrontal);

            mPreview = new CameraPreview(view.getContext(), mCamera);

            mCameraFragmentView.addView(mPreview);
        }*/


        return view;
    }

    private Camera findCamera(boolean isFrontal) {
        Camera camera = null;
        Camera.CameraInfo info = new Camera.CameraInfo();
        for(int cameraId = 0; cameraId <
                Camera.getNumberOfCameras();
                cameraId++) {
            Camera.getCameraInfo(cameraId, info);
            if(isFrontal
                    && info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                return Camera.open(cameraId);
            }else if(!isFrontal
                    && info.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                return Camera.open();
            }
        }

        return camera;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mCallback = (CameraFragmentCallback) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement CameraFragmentCallback");
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(!isVisibleToUser) {
            Log.d("visible", "fragment not visible anymore " + this);
            releaseCamera();
        }else {
            if(!mCallback.cameraIsLocked()) {
                Log.d("visible", "passou no unlock " + this);
                mCallback.lockCamera();
                mCamera = findCamera(isFrontal);

                mPreview = new CameraPreview(getContext(), mCamera);

                mCameraFragmentView.addView(mPreview);
            }
        }
    }

    public void releaseCamera() {
        if(mCamera != null) {
            mCameraFragmentView.removeView(mPreview);
            mCallback.unlockCamera();
            mCamera.stopPreview();
            mCamera.release();
            mCamera = null;
        }
    }
}
