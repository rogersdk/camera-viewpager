package com.rogersdk.frontandbackcameraviewpager;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by rogerio on 16/12/15.
 */
public class CameraSurfaceViewFragment extends Fragment {

    private static final String IS_CAMERA_FRONTAL = "isFrontal";

    public CameraSurfaceViewFragment() {

    }

    public static CameraSurfaceViewFragment newInstance(boolean isFrontal) {
        CameraSurfaceViewFragment fragment = new CameraSurfaceViewFragment();
        Bundle args = new Bundle();
        args.putBoolean(IS_CAMERA_FRONTAL, isFrontal);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Bundle args = getArguments();
        boolean isFrontal = false;
        if(!args.isEmpty()) {
            args.getBoolean(IS_CAMERA_FRONTAL, false);
        }

        return new CameraPreview(inflater.getContext(), null);
    }
}
