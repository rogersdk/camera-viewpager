package com.rogersdk.frontandbackcameraviewpager;

/**
 * Created by rogerio on 17/12/15.
 */
public interface CameraFragmentCallback {
    void lockCamera();
    void unlockCamera();
    boolean cameraIsLocked();
}
