package com.rogersdk.frontandbackcameraviewpager;

import android.app.ActionBar.TabListener;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;

/**
 * Created by rogerio on 16/12/15.
 */
public class MainActivity extends ActionBarActivity implements ActionBar.TabListener, CameraFragmentCallback{

    private ViewPager mViewPager;
    private ActionBar mActionBar;
    private PageAdapter mPageAdapter;

    private String[] tabs = { "Tab1", "Tab2", "Tab3", "Tab4" };
    private boolean cameraIsLocked;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        mViewPager = (ViewPager) findViewById(R.id.viewpager);
        mActionBar = getSupportActionBar();
        mPageAdapter = new PageAdapter(getSupportFragmentManager(),4);

        mViewPager.setAdapter(mPageAdapter);
        mActionBar.setHomeButtonEnabled(false);
        mActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        // Adding Tabs
        for (String tab_name : tabs) {
            mActionBar.addTab(mActionBar.newTab().setText(tab_name)
                    .setTabListener(this));
        }

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                if(position == 2) {
                    CameraFragment fragment = (CameraFragment) mPageAdapter.getItem(position - 1);
                    fragment.releaseCamera();
                }else if(position == 3) {
                    CameraFragment fragment = (CameraFragment) mPageAdapter.getItem(position - 1);
                    fragment.releaseCamera();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, android.support.v4.app.FragmentTransaction fragmentTransaction) {

    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, android.support.v4.app.FragmentTransaction fragmentTransaction) {

    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, android.support.v4.app.FragmentTransaction fragmentTransaction) {

    }

    @Override
    public void lockCamera() {
        Log.d("visible", "camera locked");
        cameraIsLocked = true;
    }

    @Override
    public void unlockCamera() {
        Log.d("visible", "camera unlocked");
        cameraIsLocked = false;
    }

    @Override
    public boolean cameraIsLocked() {
        return cameraIsLocked;
    }
}
