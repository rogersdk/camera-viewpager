package com.rogersdk.frontandbackcameraviewpager;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by rogerio on 16/12/15.
 */
public class PlaceHolderFragment extends Fragment {


    public PlaceHolderFragment() {

    }


    public static PlaceHolderFragment newInstance(int position) {
        PlaceHolderFragment fragment = new PlaceHolderFragment();
        Bundle args = new Bundle();
        args.putInt("position", position);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_placeholder, container, false);

        TextView title = (TextView) view.findViewById(R.id.title);

        Bundle args = getArguments();

        int position = 0;

        if(!args.isEmpty()) {
            position = args.getInt("position");
        }

        title.setText(String.format("Placeholder is %d", position));


        return view;
    }
}
