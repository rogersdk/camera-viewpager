package com.rogersdk.frontandbackcameraviewpager;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by rogerio on 16/12/15.
 */
public class PageAdapter extends FragmentPagerAdapter {

    private int totalTabs = 0;

    public PageAdapter(FragmentManager fragmentManager, int totalTabs) {
        super(fragmentManager);
        this.totalTabs = totalTabs;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment itemFragment = null;
        switch (position) {
            case 0:
                itemFragment = PlaceHolderFragment.newInstance(position);
                break;
            case 1:
                itemFragment = CameraFragment.newInstance(true);
                break;
            case 2:
                itemFragment = CameraFragment.newInstance(false);
                break;
            case 3:
                itemFragment = PlaceHolderFragment.newInstance(position);
                break;
        }
        return itemFragment;
    }

    @Override
    public int getCount() {
        return totalTabs;
    }
}
